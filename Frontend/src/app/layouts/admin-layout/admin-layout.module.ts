import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { PacienteComponent } from '../../pacientes/paciente.component';
import { MedicosComponent } from '../../medicos/medicos.component';
import { AgendaComponent } from '../../agenda/agenda.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ModalAgendaComponent } from '../../shared/components/modal-agenda/modal-agenda.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskModule } from 'ngx-mask'

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatDialogModule,
  MatTableModule,
  MatRadioModule
} from '@angular/material';
import { GlobalService } from 'app/app.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatDialogModule,
    MatTableModule,
    MatRadioModule,
    FullCalendarModule,
    HttpClientModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    PacienteComponent,
    MedicosComponent,
    AgendaComponent,
    ModalAgendaComponent
  ],
  entryComponents: [
    ModalAgendaComponent
  ],
  providers: [
    GlobalService
  ]
})

export class AdminLayoutModule { }
