import { Routes } from '@angular/router';

import { PacienteComponent } from '../../pacientes/paciente.component';
import { MedicosComponent } from '../../medicos/medicos.component';
import { AgendaComponent } from 'app/agenda/agenda.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'pacientes', component: PacienteComponent },
    { path: 'medicos', component: MedicosComponent },
    { path: 'agenda', component: AgendaComponent },
];
