import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Doctor } from 'app/shared/models/doctor.model';
import { GlobalService } from 'app/app.service';
import { Util } from 'app/shared/utils/Util';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit {

  formMedico: FormGroup;
  doctors: Doctor[] = [];

  constructor(private service: GlobalService) { }

  ngOnInit() {
    this.createForm();
    this.obterMedicos();
  }

  obterMedicos(): void {
    this.service.getAll('doctors').subscribe(
      (data: Doctor[]) => {
        this.doctors = data;
      }
    )
  }

  createForm(): void {
    this.formMedico = new FormGroup({
      'nome': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      'email': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(70)]),
      'codigo': new FormControl(null, [Validators.required, Validators.maxLength(5)])
    });
  }

  cadastrarMedico(): void {
    if (this.formMedico.valid) {
      let doctor = new Doctor(
        this.formMedico.get('nome').value,
        this.formMedico.get('email').value,
        this.formMedico.get('codigo').value,
      );

      this.service.post(doctor, 'doctors').subscribe((data) => {
        if (data.success) {
          Util.notification('primary', data.message);
          this.formMedico.reset();
          this.obterMedicos();
        }
      });
    }
  }

  excluirMedico(id: number): void {
    this.service.delete(id, 'doctors').subscribe(
      (data: any) => {
        if (data.success) {
          Util.notification('primary', data.message);
          this.obterMedicos();
        }
      })
  }
}
