import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Util } from 'app/shared/utils/Util';
import { GlobalService } from 'app/app.service';
import { Patient } from 'app/shared/models/patient.model';



@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})

export class PacienteComponent implements OnInit {

  formPaciente: FormGroup;
  patients: Patient[] = [];

  constructor(private service: GlobalService) { }

  ngOnInit() {
    this.createForm();
    this.obterPacientes();
  }

  obterPacientes(): void {
    this.service.getAll('patients').subscribe(
      (data: Patient[]) => {
        this.patients = data;
      })
  }

  createForm(): void {
    this.formPaciente = new FormGroup({
      'nome': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      'cpf': new FormControl(null, [Validators.required, Validators.minLength(11), Validators.maxLength(11)])
    })
  }

  cadastrarPaciente(): void {
    if (this.formPaciente.valid) {

      let patient = new Patient(this.formPaciente.get('nome').value, this.formPaciente.get('cpf').value);

      this.service.post(patient, 'patients').subscribe((data) => {
        if (data.success) {
          Util.notification('primary', data.message);
          this.formPaciente.reset();
          this.obterPacientes();
        }
      });
    }
  }

  excluirPaciente(id: number): void {
    this.service.delete(id, 'patients').subscribe(
      (data: any) => {
        if (data.success) {
          Util.notification('primary', data.message);
          this.obterPacientes();
        }
      })
  }

}
