import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class GlobalService {
    url: string;
    headers: HttpHeaders;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders().set('Content-Type', 'application/json');
        this.url = 'http://localhost:62135/api';
    }

    getAll(route: string): Observable<any[]> {
        return this.http.get<any[]>(`${this.url}/${route}`, { headers: this.headers });
    }

    getBydId(id: number, route: string): Observable<any> {
        return this.http.get<any>(`${this.url}/${route}/${id}`, { headers: this.headers });
    }

    post(data: any, route: string): Observable<any> {
        return this.http.post(`${this.url}/${route}`, data, { headers: this.headers });
    }

    put(data: any, route: string): Observable<any> {
        return this.http.put(`${this.url}/${route}`, data, { headers: this.headers })
    }

    delete(id: number, route: string) {
        return this.http.delete(`${this.url}/${route}/${id}`);
    }
}