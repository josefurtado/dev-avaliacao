import { Component, OnInit, ViewChild } from '@angular/core';
import { OptionsInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FullCalendarComponent } from '@fullcalendar/angular';
import interactionPlugin from '@fullcalendar/interaction';
import { MatDialog } from '@angular/material/dialog';
import { ScheduleItem } from 'app/shared/models/scheduleItem.model';
import { GlobalService } from 'app/app.service';
import { EventInput } from '@fullcalendar/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Util } from 'app/shared/utils/Util';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styles: []
})

export class AgendaComponent implements OnInit {
  animal: string;
  name: string;
  options: OptionsInput;
  calendarEvents: EventInput[] = [];
  formAgenda: FormGroup;

  @ViewChild('fullcalendar', { static: false }) fullcalendar: FullCalendarComponent;

  constructor(public dialog: MatDialog, private service: GlobalService) { }

  ngOnInit() {
    this.options = {
      editable: true,
      header: {
        left: 'prev,next today myCustomButton',
        center: 'title',
        right: 'dayGridMonth'
      },
      plugins: [dayGridPlugin, interactionPlugin]
    };

    this.obterEventos();
    this.createForm();
  }

  createForm(): void {
    this.formAgenda = new FormGroup({
      'descricao': new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
      'dataEvento': new FormControl(null, [Validators.required]),
      'tipoEvento': new FormControl('1')
    });
  }

  cadastrarItem(): void {
    let scheduleItem = new ScheduleItem(
      this.formAgenda.get('dataEvento').value,
      this.formAgenda.get('descricao').value,
      Number(this.formAgenda.get('tipoEvento').value),
    )

    this.service.post(scheduleItem, 'schedule').subscribe((data) => {
      if (data.success) {
        Util.notification('success', data.message);
        this.obterEventos();
        this.formAgenda.reset();
      }
    })
  }

  obterEventos(): void {
    this.service.getAll('schedule').subscribe(
      (data: ScheduleItem[]) => {
        if (data.length > 0)
          this.calendarEvents = [];

        data.map((item) => {
          if (item.scheduleType == 1)
            this.calendarEvents = this.calendarEvents.concat({ title: item.description, start: new Date(item.creationDate), color: '#00CC22' });
          else
            this.calendarEvents = this.calendarEvents.concat({ title: item.description, start: new Date(item.creationDate), color: '#00AAFF' });
        })
      });
  }

  eventClick(model) {
    console.log(model);
  }

}

