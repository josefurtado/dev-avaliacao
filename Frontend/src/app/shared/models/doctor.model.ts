export class Doctor {

    constructor(name: string, email: string, code: string) {
        this.name = name;
        this.email = email;
        this.code = code;
    }

    id: number;
    isDeleted: boolean;
    creationDate: Date;
    name: string;
    email: string;
    code: string;
}