export class Patient {

    constructor(name: string, document: string) {
        this.name = name;
        this.document = document;
    }

    id: number;
    isDeleted: boolean;
    creationDate: Date;
    name: string;
    document: string;
}