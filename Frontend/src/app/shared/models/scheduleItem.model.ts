import { Doctor } from "./doctor.model";
import { Patient } from "./patient.model";

export class ScheduleItem {

    constructor(creationDate: Date, description: string, scheduleType: number) {
        this.creationDate = creationDate;
        this.description = description;
        this.scheduleType = scheduleType;
    }

    id: number;
    isDeleted: boolean;
    creationDate: Date;
    description: string;
    scheduleType: number;
    doctorId: number;
    doctor: Doctor;

    patientId: number;
    patient: Patient;
}