import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-modal-agenda',
  templateUrl: './modal-agenda.component.html',
  styles: []
})
export class ModalAgendaComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ModalAgendaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

}
