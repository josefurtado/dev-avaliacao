# [Projeto de Avaliação SimCo]


## Imagens da aplicação

![alt text](Images/00.png)
![alt text](Images/01.png)
![alt text](Images/02.png)
![alt text](Images/03.png)



## Licença utilizada no template

- Copyright 2018 Creative Tim (https://www.creative-tim.com/)

- Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-angular2/blob/master/LICENSE.md)
