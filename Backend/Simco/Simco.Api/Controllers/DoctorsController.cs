﻿using Microsoft.AspNetCore.Mvc;
using Simco.Application.DTO;
using Simco.Application.Services.Interfaces;
using Simco.Application.Shared;

namespace Simco.Api.Controllers
{
    [ApiController]
    public class DoctorsController : ControllerBase
    {
        private IDoctorService _service;

        public DoctorsController(IDoctorService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("api/doctors")]
        public ActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpGet]
        [Route("api/doctors/{id:int}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_service.GetById(id));
        }

        [HttpPost]
        [Route("api/doctors")]
        public ActionResult Save([FromBody]DoctorDTO doctor)
        {
            if (!doctor.IsValid())
                return Ok(new DefaultResponse
                {
                    Success = false,
                    Message = "Erro ao salvar médico",
                    Data = doctor.Notifications
                }
                );

            var result = _service.Save(doctor);

            return Ok(result);
        }

        [HttpPut]
        [Route("api/doctors")]
        public ActionResult Update([FromBody]DoctorDTO doctor)
        {
            if (!doctor.IsValid())
                return Ok(new DefaultResponse
                {
                    Success = false,
                    Message = "Erro ao editar médico",
                    Data = doctor.Notifications
                }
                );

            var result = _service.Update(doctor);

            return Ok(result);
        }

        [HttpDelete]
        [Route("api/doctors/{id:int}")]
        public ActionResult Delete(int id)
        {
            var result = _service.Delete(id);

            return Ok(result);
        }
    }
}