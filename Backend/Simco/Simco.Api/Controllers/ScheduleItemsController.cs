﻿using Microsoft.AspNetCore.Mvc;
using Simco.Application.DTO;
using Simco.Application.Services.Interfaces;
using Simco.Application.Shared;

namespace Simco.Api.Controllers
{
    [ApiController]
    public class ScheduleItemsController : ControllerBase
    {
        private IScheduleItemService _service;

        public ScheduleItemsController(IScheduleItemService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("api/schedule")]
        public ActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpGet]
        [Route("api/schedule/{id:int}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_service.GetById(id));
        }

        [HttpPost]
        [Route("api/schedule")]
        public ActionResult Save([FromBody]ScheduleItemDTO item)
        {
            if (!item.IsValid())
                return Ok(new DefaultResponse
                {
                    Success = false,
                    Message = "Erro ao salvar item",
                    Data = item.Notifications
                });

            var result = _service.Save(item);

            return Ok(result);
        }

        [HttpPut]
        [Route("api/schedule")]
        public ActionResult Update([FromBody]ScheduleItemDTO item)
        {
            if (!item.IsValid())
                return Ok(new DefaultResponse
                {
                    Success = false,
                    Message = "Erro ao editar item",
                    Data = item.Notifications
                });

            var result = _service.Update(item);

            return Ok(result);
        }
    }
}