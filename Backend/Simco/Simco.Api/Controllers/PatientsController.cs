﻿using Microsoft.AspNetCore.Mvc;
using Simco.Application.DTO;
using Simco.Application.Services.Interfaces;
using Simco.Application.Shared;

namespace Simco.Api.Controllers
{
    [ApiController]
    public class PatientsController : ControllerBase
    {
        private IPatientService _service;

        public PatientsController(IPatientService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("api/patients")]
        public ActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpGet]
        [Route("api/patients/{id:int}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_service.GetById(id));
        }

        [HttpPost]
        [Route("api/patients")]
        public ActionResult Save([FromBody]PatientDTO patient)
        {
            if (!patient.IsValid())
                return Ok(new DefaultResponse
                {
                    Success = false,
                    Message = "Erro ao salvar paciente",
                    Data = patient.Notifications
                }
                );

            var result = _service.Save(patient);

            return Ok(result);
        }

        [HttpPut]
        [Route("api/patients")]
        public ActionResult Update([FromBody]PatientDTO patient)
        {
            if (!patient.IsValid())
                return Ok(new DefaultResponse
                {
                    Success = false,
                    Message = "Erro ao editar paciente",
                    Data = patient.Notifications
                }
                );

            var result = _service.Update(patient);

            return Ok(result);
        }

        [HttpDelete]
        [Route("api/patients/{id:int}")]
        public ActionResult Delete(int id)
        {
            var result = _service.Delete(id);

            return Ok(result);
        }
    }
}