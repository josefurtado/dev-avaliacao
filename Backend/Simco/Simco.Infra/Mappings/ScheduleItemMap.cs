﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simco.Domain.Entities;

namespace Simco.Infra.Mappings
{
    public class ScheduleItemMap : IEntityTypeConfiguration<ScheduleItem>
    {
        public void Configure(EntityTypeBuilder<ScheduleItem> builder)
        {
            builder.Property(x => x.Id);

            builder.Property(x => x.IsDeleted);

            builder.Property(x => x.CreationDate);

            builder.Property(x => x.ScheduleType);

            builder.Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(200);

            builder.HasOne<Doctor>(x => x.Doctor)
                .WithMany()
                .HasForeignKey("DoctorId");

            builder.HasOne<Patient>(x => x.Patient)
                .WithMany()
                .HasForeignKey("PatientId");

        }
    }
}
