﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simco.Domain.Entities;

namespace Simco.Infra.Mappings
{
    public class DoctorMap : IEntityTypeConfiguration<Doctor>
    {
        public void Configure(EntityTypeBuilder<Doctor> builder)
        {
            builder.Property(x => x.Id);

            builder.Property(x => x.IsDeleted);

            builder.Property(x => x.CreationDate);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(70);

            builder.Property(x => x.Code)
                .IsRequired()
                .HasMaxLength(5);
        }
    }
}
