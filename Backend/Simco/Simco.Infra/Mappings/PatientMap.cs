﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Simco.Domain.Entities;

namespace Simco.Infra.Mappings
{
    public class PatientMap : IEntityTypeConfiguration<Patient>
    {
        public void Configure(EntityTypeBuilder<Patient> builder)
        {
            builder.Property(x => x.Id);

            builder.Property(x => x.IsDeleted);

            builder.Property(x => x.CreationDate);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Document)
                .IsRequired()
                .HasMaxLength(11);
        }
    }
}
