﻿using Microsoft.EntityFrameworkCore;
using Simco.Domain.Entities;
using Simco.Infra.Mappings;

namespace Simco.Infra.Context
{
    public class SimContext : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<ScheduleItem> ScheduleItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(@"Host=localhost;Port=5432;Username=postgres;Password=yourpass;Database=SiMDatabase;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new DoctorMap());
            modelBuilder.ApplyConfiguration(new PatientMap());
            modelBuilder.ApplyConfiguration(new ScheduleItemMap());

            base.OnModelCreating(modelBuilder);
        }

    }
}
