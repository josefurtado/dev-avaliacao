﻿using Microsoft.EntityFrameworkCore;
using Simco.Domain.Entities;
using Simco.Domain.Interfaces.Repositories;
using Simco.Infra.Context;
using System.Linq;

namespace Simco.Infra.Repositories
{
    public class DoctorRepository : IDoctorRepository
    {
        private SimContext _context;

        public DoctorRepository(SimContext context)
        {
            _context = context;
        }

        public void Add(Doctor entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.Doctors.Remove(Get().FirstOrDefault(x => x.Id == id));
            _context.SaveChanges();
        }

        public IQueryable<Doctor> Get()
        {
            return _context.Doctors;
        }

        public void Update(Doctor entity)
        {
            _context.Entry<Doctor>(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
