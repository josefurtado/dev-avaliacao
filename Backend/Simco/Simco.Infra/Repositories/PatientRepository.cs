﻿using Microsoft.EntityFrameworkCore;
using Simco.Domain.Entities;
using Simco.Domain.Interfaces.Repositories;
using Simco.Infra.Context;
using System.Linq;

namespace Simco.Infra.Repositories
{
    public class PatientRepository : IPatientRepository
    {
        private SimContext _context;

        public PatientRepository(SimContext context)
        {
            _context = context;
        }

        public void Add(Patient entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.Patients.Remove(Get().FirstOrDefault(x => x.Id == id));
            _context.SaveChanges();
        }

        public IQueryable<Patient> Get()
        {
            return _context.Patients;
        }

        public void Update(Patient entity)
        {
            _context.Entry<Patient>(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
