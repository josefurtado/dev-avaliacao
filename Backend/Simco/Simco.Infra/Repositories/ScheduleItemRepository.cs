﻿using Microsoft.EntityFrameworkCore;
using Simco.Domain.Entities;
using Simco.Domain.Interfaces.Repositories;
using Simco.Infra.Context;
using System.Linq;

namespace Simco.Infra.Repositories
{
    public class ScheduleItemRepository : IScheduleItemRepository
    {
        private SimContext _context;

        public ScheduleItemRepository(SimContext context)
        {
            _context = context;
        }

        public void Add(ScheduleItem entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            _context.ScheduleItems.Remove(Get().FirstOrDefault(x => x.Id == id));
            _context.SaveChanges();
        }

        public IQueryable<ScheduleItem> Get()
        {
            return _context.ScheduleItems;
        }

        public void Update(ScheduleItem entity)
        {
            _context.Entry<ScheduleItem>(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
