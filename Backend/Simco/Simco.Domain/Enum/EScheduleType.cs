﻿namespace Simco.Domain.Enum
{
    public enum EScheduleType
    {
        MedicalAppointment = 1,
        Reminder = 2
    }
}
