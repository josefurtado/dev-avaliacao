﻿using System;

namespace Simco.Domain.Entities
{
    public class Doctor : Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Email { get; set; }
    }
}
