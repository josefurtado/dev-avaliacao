﻿using System;

namespace Simco.Domain.Entities
{
    public class Patient : Entity
    {
        public string Name { get; set; }
        public string Document { get; set; }
    }
}
