﻿using Simco.Domain.Enum;

namespace Simco.Domain.Entities
{
    public class ScheduleItem : Entity
    {
        public string Description { get; set; }
        public EScheduleType ScheduleType { get; set; }

        public int? DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }

        public int? PatientId { get; set; }
        public virtual Patient Patient { get; set; }
    }
}
