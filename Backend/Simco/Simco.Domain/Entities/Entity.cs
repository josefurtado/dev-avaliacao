﻿using System;

namespace Simco.Domain.Entities
{
    public class Entity 
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
