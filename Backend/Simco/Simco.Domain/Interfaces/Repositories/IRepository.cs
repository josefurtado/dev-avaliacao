﻿using System.Linq;

namespace Simco.Domain.Interfaces.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Get();
        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
