﻿using Simco.Domain.Entities;

namespace Simco.Domain.Interfaces.Repositories
{
    public interface IPatientRepository : IRepository<Patient>
    {
    }
}
