﻿using Simco.Application.DTO;

namespace Simco.Application.Services.Interfaces
{
    public interface IDoctorService : IService<DoctorDTO>
    {
    }
}
