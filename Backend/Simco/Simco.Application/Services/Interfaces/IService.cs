﻿using Simco.Application.Shared;
using System.Collections.Generic;

namespace Simco.Application.Services.Interfaces
{
    public interface IService<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        DefaultResponse Save(T item);
        DefaultResponse Update(T item);
        DefaultResponse Delete(int id);
    }
}
