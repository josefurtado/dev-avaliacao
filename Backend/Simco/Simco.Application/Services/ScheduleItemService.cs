﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Simco.Application.DTO;
using Simco.Application.Services.Interfaces;
using Simco.Application.Shared;
using Simco.Domain.Entities;
using Simco.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simco.Application.Services
{
    public class ScheduleItemService : IScheduleItemService
    {
        private IScheduleItemRepository _repository;
        private IMapper _mapper;

        public ScheduleItemService(IScheduleItemRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        public DefaultResponse Delete(int id)
        {
            try
            {
                _repository.Delete(id);

                return new DefaultResponse { Success = true, Message = "Item removido com sucesso." };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message };
            }
        }

        public IEnumerable<ScheduleItemDTO> GetAll()
        {
            var items = _repository
                .Get()
                .AsNoTracking()
                .Include(x => x.Doctor)
                .Include(x => x.Patient)
                .ToList();

            return _mapper.Map<IEnumerable<ScheduleItem>, IEnumerable<ScheduleItemDTO>>(items);
        }

        public ScheduleItemDTO GetById(int id)
        {
            var item = _repository.Get().FirstOrDefault(x => x.Id == id);

            return _mapper.Map<ScheduleItem, ScheduleItemDTO>(item);
        }

        public DefaultResponse Save(ScheduleItemDTO item)
        {
            try
            {
                _repository.Add(_mapper.Map<ScheduleItemDTO, ScheduleItem>(item));

                return new DefaultResponse { Success = true, Message = "Item salvo com sucesso.", Data = item };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message, Data = item };
            }
        }

        public DefaultResponse Update(ScheduleItemDTO item)
        {
            try
            {
                _repository.Update(_mapper.Map<ScheduleItemDTO, ScheduleItem>(item));

                return new DefaultResponse { Success = true, Message = "Item atualizado com sucesso.", Data = item };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message, Data = item };
            }
        }
    }
}
