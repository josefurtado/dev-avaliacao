﻿using AutoMapper;
using Simco.Application.DTO;
using Simco.Application.Services.Interfaces;
using Simco.Application.Shared;
using Simco.Domain.Entities;
using Simco.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simco.Application.Services
{
    public class PatientService : IPatientService
    {
        private IPatientRepository _repository;
        private IMapper _mapper;

        public PatientService(IPatientRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        public DefaultResponse Delete(int id)
        {
            try
            {
                _repository.Delete(id);

                return new DefaultResponse { Success = true, Message = "Paciente removido com sucesso." };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message };
            }
        }

        public IEnumerable<PatientDTO> GetAll()
        {
            var patients = _repository.Get().ToList();

            return _mapper.Map<IEnumerable<Patient>, IEnumerable<PatientDTO>>(patients);
        }

        public PatientDTO GetById(int id)
        {
            var patient = _repository.Get().FirstOrDefault(x => x.Id == id);

            return _mapper.Map<Patient, PatientDTO>(patient);
        }

        public DefaultResponse Save(PatientDTO item)
        {
            try
            {
                _repository.Add(_mapper.Map<PatientDTO, Patient>(item));

                return new DefaultResponse { Success = true, Message = "Paciente salvo com sucesso.", Data = item };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message, Data = item };
            }
        }

        public DefaultResponse Update(PatientDTO item)
        {
            try
            {
                _repository.Update(_mapper.Map<PatientDTO, Patient>(item));

                return new DefaultResponse { Success = true, Message = "Paciente atualizado com sucesso.", Data = item };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message, Data = item };
            }
        }
    }
}
