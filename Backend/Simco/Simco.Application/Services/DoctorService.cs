﻿using AutoMapper;
using Simco.Application.DTO;
using Simco.Application.Services.Interfaces;
using Simco.Application.Shared;
using Simco.Domain.Entities;
using Simco.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Simco.Application.Services
{
    public class DoctorService : IDoctorService
    {
        private IDoctorRepository _repository;
        private IMapper _mapper;

        public DoctorService(IDoctorRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public DefaultResponse Delete(int id)
        {
            try
            {
                _repository.Delete(id);

                return new DefaultResponse { Success = true, Message = "Médico removido com sucesso." };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message };
            }
        }

        public IEnumerable<DoctorDTO> GetAll()
        {
            var doctors = _repository.Get();

            return _mapper.Map<IEnumerable<Doctor>, IEnumerable<DoctorDTO>>(doctors);
        }

        public DoctorDTO GetById(int id)
        {
            var doctor = _repository.Get().FirstOrDefault(x => x.Id == id);

            return _mapper.Map<Doctor, DoctorDTO>(doctor);
        }

        public DefaultResponse Save(DoctorDTO item)
        {
            try
            {
                _repository.Add(_mapper.Map<DoctorDTO, Doctor>(item));

                return new DefaultResponse { Success = true, Message = "Médico salvo com sucesso.", Data = item };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message, Data = item };
            }
        }

        public DefaultResponse Update(DoctorDTO item)
        {
            try
            {
                _repository.Update(_mapper.Map<DoctorDTO, Doctor>(item));

                return new DefaultResponse { Success = true, Message = "Médico atualizado com sucesso.", Data = item };
            }
            catch (Exception ex)
            {
                return new DefaultResponse { Success = false, Message = ex.Message, Data = item };
            }
        }
    }
}
