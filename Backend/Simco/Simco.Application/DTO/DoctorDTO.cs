﻿using Flunt.Notifications;
using Flunt.Validations;
using System;

namespace Simco.Application.DTO
{
    public class DoctorDTO : Notifiable
    {
        public DoctorDTO()
        {
            CreationDate = DateTime.Now;
        }

        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreationDate { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }

        public bool IsValid()
        {
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(Name, "Name", "O nome deve ser informado.")
                .IsNotNullOrEmpty(Email, "Email", "O email deve ser informado.")
                .IsNotNullOrEmpty(Code, "Code", "O código deve ser informado.")
            );

            return Valid;
        }
    }
}
