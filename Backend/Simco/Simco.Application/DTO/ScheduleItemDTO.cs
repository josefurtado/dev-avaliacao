﻿using Flunt.Notifications;
using Flunt.Validations;
using Simco.Domain.Enum;
using System;

namespace Simco.Application.DTO
{
    public class ScheduleItemDTO : Notifiable
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }
        public EScheduleType ScheduleType { get; set; }

        public int? DoctorId { get; set; }
        public virtual DoctorDTO Doctor { get; set; }

        public int? PatientId { get; set; }
        public virtual PatientDTO Patient { get; set; }

        public bool IsValid()
        {
            AddNotifications(new Contract()
                .IsFalse(CreationDate == DateTime.MinValue || CreationDate == DateTime.MaxValue, "CreationDate", "A data de criação é inválida.")
                .IsNotNullOrEmpty(Description, "Description", "A descrição deve ser informada.")
            );

            return Valid;
        }
    }
}
