﻿using Flunt.Notifications;
using Flunt.Validations;
using System;

namespace Simco.Application.DTO
{
    public class PatientDTO : Notifiable
    {
        public PatientDTO()
        {
            CreationDate = DateTime.Now;
        }

        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name { get; set; }
        public string Document { get; set; }
        public DateTime CreationDate { get; set; }

        public bool IsValid()
        {
            AddNotifications(new Contract()
                .IsNotNullOrEmpty(Name, "Name", "O nome deve ser informado.")
                .IsNotNullOrEmpty(Document, "Document", "O email deve ser informado.")
            );

            return Valid;
        }
    }
}
